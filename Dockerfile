# This image will be used for running Bitbucket Pipelines
# Based on https://hub.docker.com/r/phpdockerio/php80-cli

FROM phpdockerio/php:8.0-cli

RUN /bin/bash && apt-get update && \
  curl -sL https://deb.nodesource.com/setup_10.x | sh && \
  apt-get install nodejs -y && \
  curl -o- -L https://yarnpkg.com/install.sh | bash

RUN apt-get update \
  && apt-get -y --no-install-recommends install curl vim ca-certificates unzip \
  php8.0-gd php8.0-bcmath php8.0-intl php8.0-sqlite3 php8.0-zip php8.0-gd \
  && apt-get clean

#RUN groupadd -g 999 appuser && \
#    useradd -r -u 999 -g appuser appuser
#USER appuser

CMD ["php", "-a"]

# If you'd like to be able to use this container on a docker-compose environment as a quiescent PHP CLI container
# you can /bin/bash into, override CMD with the following - bear in mind that this will make docker-compose stop
# slow on such a container, docker-compose kill might do if you're in a hurry
# CMD ["tail", "-f", "/dev/null"]
