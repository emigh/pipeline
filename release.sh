#!/bin/sh

# https://medium.com/better-programming/how-to-version-your-docker-images-1d5c577ebf54

set -ex
# SET THE FOLLOWING VARIABLES
# docker hub username
USERNAME=alsbury
# image name
IMAGE=emigh-pipeline
BUMP_DEFAULT="patch"
BUMP="${BUMP:=$BUMP_DEFAULT}"
# ensure we're up to date
git pull
# bump version
docker run --rm -v "$PWD":/app treeder/bump $BUMP
version=`cat VERSION`
echo "version: $version"
# run build
./build.sh
# tag it
git add -A
git commit -m "version $version"
git tag -a "$version" -m "version $version"
git push
git push --tags
docker tag $USERNAME/$IMAGE:latest $USERNAME/$IMAGE:$version
# push it
docker push $USERNAME/$IMAGE:latest
docker push $USERNAME/$IMAGE:$version
