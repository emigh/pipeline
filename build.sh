#!/bin/sh

set -ex
# SET THE FOLLOWING VARIABLES
# docker hub username
USERNAME=alsbury
# image name
IMAGE=emigh-pipeline
docker build -t $USERNAME/$IMAGE:latest .
